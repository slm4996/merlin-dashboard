<?php 
# The nagios-dashboard was written by Morten Bekkelund & Jonas G. Drange in 2010
#
# Patched, modified and added to by various people, see README
# Maintained as merlin-dashboard by Mattias Bergsten <mattias.bergsten@op5.com>
#
# Parts copyright (C) 2010 Morten Bekkelund & Jonas G. Drange
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# See: http://www.gnu.org/copyleft/gpl.html

$con = mysql_connect("localhost", "merlin", "merlin") or die("<h3><font color=red>Could not connect to the Merlin database!</font></h3>");
$db = mysql_select_db("icinga", $con);
$prefix = "icinga_";

function dateDiff($start_time, $end_time) {
    $start = new DateTime($start_time);
    $interval = $start->diff(new DateTime($end_time));
    return $interval->format('%a days, %h hours, %i minutes'); 
}

?>
<div class="dash_unhandled_hosts hosts dash">
    <div class="dash_tactical_overview tactical_overview hosts dash">
        <div class="dash_wrapper">
            <table class="dash_table">
                <?php 
            # number of hosts down
                $query = "select count(1) as count from ".$prefix."hoststatus where current_state = 1";
                $result = mysql_query($query);
                $row = mysql_fetch_array($result);
                $hosts_down = $row[0];

            # number of hosts unreachable
                $query = "select count(1) as count from ".$prefix."hoststatus where current_state = 2";
                $result = mysql_query($query);
                $row = mysql_fetch_array($result);
                $hosts_unreach = $row[0];

            # total number of hosts
                $query = "select count(1) as count from ".$prefix."hosts";
                $result = mysql_query($query);
                $row = mysql_fetch_array($result);
                $total_hosts = $row[0];

                $hosts_down_pct = round($hosts_down / $total_hosts * 100, 2);
                $hosts_unreach_pct = round($hosts_unreach / $total_hosts * 100, 2);
                $hosts_up = $total_hosts - ($hosts_down + $hosts_unreach);
                $hosts_up_pct = round($hosts_up / $total_hosts * 100, 2);           

                print "<tr><td class=\"ok total_hosts_up nowrap\">Up | $hosts_up/$total_hosts | $hosts_up_pct%</td>";
                if ($hosts_down > 0) {
                  $class = "critical";
              } else {
                  $class = "ok";
              };
              print "<td class=\"$class total_hosts_down nowrap\">Down | $hosts_down/$total_hosts | $hosts_down_pct%</td>";
              if ($hosts_unreach > 0) {
                  $class = "unreachable";
              } else {
                  $class = "ok";
              };
              print "<td class=\"$class total_hosts_unreach nowrap\">Unreachable | $hosts_unreach/$total_hosts | $hosts_unreach_pct%</td>";
              ?>
          </tr>
      </table>
  </div>
</div>
<h2 class="hide left">Unhandled&nbsp;</h2><h2 class="left">host problems</h2>
<div class="clear"></div>
<div class="dash_wrapper">
    <table class="dash_table">
        <?php 
        $query = "SELECT
        ".$prefix."hosts.address,
        ".$prefix."hosts.alias,
        count( ".$prefix."hosts.alias),
        ".$prefix."hoststatus.current_state,
        ".$prefix."hoststatus.last_hard_state,
        parent_hosts.address,
        parent_hoststatus.current_state
        FROM
        ".$prefix."hoststatus INNER JOIN ".$prefix."hosts ON ".$prefix."hoststatus.host_object_id = ".$prefix."hosts.host_object_id
        INNER JOIN ".$prefix."host_parenthosts ON ".$prefix."hosts.host_id = ".$prefix."host_parenthosts.host_id
        INNER JOIN ".$prefix."hosts parent_hosts ON ".$prefix."host_parenthosts.parent_host_object_id = parent_hosts.host_object_id
        INNER JOIN icinga_hoststatus parent_hoststatus ON parent_hosts.host_object_id = parent_hoststatus.host_object_id
        WHERE
        ".$prefix."hoststatus.current_state IN (1,2,3)
        AND ".$prefix."hoststatus.problem_has_been_acknowledged = 0
        AND ".$prefix."hosts.host_object_id NOT IN ( SELECT DISTINCT ".$prefix."scheduleddowntime.object_id FROM ".$prefix."scheduleddowntime WHERE ".$prefix."scheduleddowntime.scheduled_start_time < unix_timestamp() AND ".$prefix."scheduleddowntime.scheduled_end_time > unix_timestamp() AND ".$prefix."scheduleddowntime.downtime_type = 2 )
        GROUP BY
        ".$prefix."hosts.alias;";
        $result = mysql_query($query);
        $save = "";
        $output = "";
        while ($row = mysql_fetch_array($result)) {
            if ($row[3] == 3) {
                $class = "unknown";
            } elseif ($row[3] == 1) {
                $class = "critical";
            } elseif ($row[3] == 2) {
                $class = "unreachable";
            } 
            if ($row[6] == 3) {
                $pclass = "unknown";
            } elseif ($row[6] == 1) {
                $pclass = "critical";
            } elseif ($row[6] == 2) {
                $pclass = "unreachable";
            } elseif ($row[6] == 0) {
                $pclass = "ok";
            }
            $output .=  "<tr class=\"".$class." nowrap\"><td>".$row[0]."</td><td class=\"nowrap\">".$row[1]."</td><td class=\"".$pclass." nowrap\">".$row[5]."</td></tr>";
            $save .= $row[0];
        }
        if($save):
            ?>
        <tr class="dash_table_head">
            <th>Hostname/IP</th>
            <th>Alias/Description</th>
            <th>Parent device</th>
        </tr>
        <?php print $output; ?>
        <?php
        else: 
            print "<tr class=\"ok\"><td>No hosts down or unacknowledged</td></tr>";
        endif;
        ?>
    </table>
</div>
</div>
<div class="clear"><br/></div>
<div class="dash_unhandled_service_problems hosts dash">
    <div class="dash_tactical_overview tactical_overview hosts dash">
        <div class="dash_wrapper">
            <table class="dash_table">
                <?php 
            #### SERVICES
            #
            # critical
                $query = "SELECT count(1) AS count FROM ".$prefix."servicestatus INNER JOIN ".$prefix."services ON ".$prefix."servicestatus.service_object_id = ".$prefix."services.service_object_id WHERE ".$prefix."servicestatus.last_hard_state = 2";
                $result = mysql_query($query);
                $row = mysql_fetch_array($result);
                $services_critical = $row[0];

            # warning
                $query = "SELECT count(1) AS count FROM ".$prefix."servicestatus INNER JOIN ".$prefix."services ON ".$prefix."servicestatus.service_object_id = ".$prefix."services.service_object_id WHERE ".$prefix."servicestatus.last_hard_state = 1";
                $result = mysql_query($query);
                $row = mysql_fetch_array($result);
                $services_warning = $row[0];

            # total number of services
                $query = "select count(1) as count from ".$prefix."services";
                $result = mysql_query($query);
                $row = mysql_fetch_array($result);
                $total_services = $row[0];

                $services_critical_pct = round($services_critical / $total_services * 100, 2);
                $services_warning_pct = round($services_warning / $total_services * 100, 2);
                $services_ok = $total_services - ($services_critical + $services_warning);
                $services_ok_pct = round($services_ok / $total_services * 100, 2);

                ?>
                <tr class="ok total_services_ok">
                    <td class="nowrap"> OK | <?php print $services_ok ?>/<?php print $total_services ?> | <?php print $services_ok_pct ?>%</td>
                    <?php if ($services_critical > 0) {
                        $class = "critical";
                    } else {
                        $class = "ok";
                    };
                    print "<td class=\"$class total_services_critical nowrap\"> Critical | $services_critical/$total_services | $services_critical_pct%</td>";

                    if ($services_warning > 0) {
                        $class = "warning";
                    } else {
                        $class = "ok";
                    };
                    print "<td class=\"$class total_services_warning nowrap\"> Warning | $services_warning/$total_services | $services_warning_pct%</td>";
                    ?>

                </tr>
            </table>
        </div>
    </div>
    <h2 class="hide left">Unhandled&nbsp;</h2><h2 class="left">service problems</h2>
    <div class="clear"></div>
    <div class="dash_wrapper">
        <table class="dash_table">
            <?php 
            $query = "SELECT
            ".$prefix."hosts.address,
            ".$prefix."services.display_name,
            ".$prefix."servicestatus.last_hard_state,
            ".$prefix."servicestatus.output,
            ".$prefix."servicestatus.last_hard_state_change,
            ".$prefix."servicestatus.last_check
            FROM
            ".$prefix."hosts INNER JOIN ".$prefix."services ON ".$prefix."hosts.host_object_id = ".$prefix."services.host_object_id
            INNER JOIN ".$prefix."servicestatus ON ".$prefix."services.service_object_id = ".$prefix."servicestatus.service_object_id
            WHERE
            ".$prefix."servicestatus.last_hard_state IN (1,2,3)
            AND ".$prefix."servicestatus.problem_has_been_acknowledged = 0
            AND ".$prefix."services.service_object_id NOT IN ( SELECT DISTINCT ".$prefix."scheduleddowntime.object_id FROM ".$prefix."scheduleddowntime WHERE ".$prefix."scheduleddowntime.scheduled_start_time < unix_timestamp() AND ".$prefix."scheduleddowntime.scheduled_end_time > unix_timestamp() AND ".$prefix."scheduleddowntime.downtime_type = 1 )
            AND ".$prefix."hosts.host_object_id NOT IN ( SELECT DISTINCT ".$prefix."scheduleddowntime.object_id FROM ".$prefix."scheduleddowntime WHERE ".$prefix."scheduleddowntime.scheduled_start_time < unix_timestamp() AND ".$prefix."scheduleddowntime.scheduled_end_time > unix_timestamp() AND ".$prefix."scheduleddowntime.downtime_type = 2 )                   
            ORDER BY
            ".$prefix."servicestatus.last_hard_state;";

            $result = mysql_query($query);

            $save = "";
            $output = "";

            while ($row = mysql_fetch_array($result)) {
                if ($row[2] == 2) {
                    $class = "critical";
                } elseif ($row[2] == 1) {
                    $class = "warning";
                } elseif ($row[2] == 3) {
                    $class = "unknown";
                }

                $duration = dateDiff($row[4], date());
                $last_time = new DateTime($row[5]);
                $date = $last_time->format('M,j,Y H:i'); 

                $output .= "<tr class=\"".$class."\"><td class=\"top\">".$row[0]."</td><td class=\"top\">".$row[1]."</td>";
                $output .= "<td class=\"date date_statechange nowrap top\">".$duration."</td>";
                $output .= "<td class=\"date date_lastcheck nowrap top\">".$date."</td></tr>";
                $output .= "<tr class=\"".$class." bottom center\"><td colspan=\"4\">".$row[3]."</td></tr>\n";
                $save .= $row[0];
            };

            if ($save):
             ?>
         <tr class="dash_table_head">
            <th>
                Host
            </th>
            <th>
                Service
            </th>
            <th>
                Duration
            </th>
            <th>
                Last check
            </th>
        </tr>
        <?php print $output; ?>
        <?php
        else:
            print "<tr class=\"ok\"><td>No services on up hosts in a problem state or unacknowledged</td></tr>";
        endif;
        ?>
    </table>
</div>
</div>
</body>
</html>
