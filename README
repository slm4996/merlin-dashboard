README for merlin-dashboard
===========================
This dashboard for Nagios using the Merlin backend was originally made by Morten Bekkelund <bekkelund@gmail.com> and made available at http://dingleberry.me/2010/04/our-new-dashboard/ and later at https://github.com/mortis1337/nagios-dashboard where several people forked it.

This is a fork designed to be used with the icinga system. It is also optimized to allow more hosts and services to be shown at once.

Authors
=======
Original project by Morten Bekkelund <bekkelund@gmail.com>.

This project maintained by Mattias Bergsten <mattias.bergsten@op5.com>.

Patches from the following list of fine people and companies:

* Jonas Drange Gr�n�s <jonas@drange.net>
* IPNett AS <http://www.ipnett.no>
* Peter Andersson <peter@it-slav.net>
* Advance AB <http://www.advance.se>
* Mattias Bergsten <mattias.bergsten@op5.com>
* John Carehag <john.carehag@westbahr.com>

License
=======
Morten's code says "GPL" but unspecified version. I'm assuming GPLv2.

Requirements
============
* Webserver with PHP5 and MySQL support
* Nagios, a version that works with the Merlin version you're running
* Merlin, anything > 1.0.0 and < 2.0 **

** In Merlin 2.0, saving status data to the database is turned off by default. This will break the dashboard. I am currently implementing a livestatus backend for merlin-dashboard, but it will not be ready in time for release of Merlin 2.0. If you do not mind the performance penalty of writing status data to the database, you can turn it on again by adding "track_current = yes" and an import program to your merlin.conf.
 
Installation
============
Put the files in a directory on your webserver.

Edit merlin.php and change the server, username and password to fit your environment.